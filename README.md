# BeeData studies

This is where I'll keep data and code related to
[BeeData](https://bcc.ime.usp.br/principal/miscelanea/grupos_extensao/beedata.html).
Some notebooks will only exist on kaggle/colab, but I'll keep any local files
here.
